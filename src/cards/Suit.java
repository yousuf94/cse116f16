package cards;

public enum Suit 
{
	WEAPONS("Weapons"),
	ROOMS("Rooms"),
	PLAYERS("Players");
	
	private final String suitText;
	private Suit(String suitText)
	{
		this.suitText = suitText;
	}
	public String printSuit()
	{
		return suitText;
	}
	
	
}
