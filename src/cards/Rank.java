package cards;

public enum Rank 
{
	BALLROOM(1, "Ballroom"), BILLIARDROOM(2, "BilliardRoom"), CONSERVATORY(3, "Conservatory"),
	DININGROOM(4, "DiningRoom"), HALL(5, "Hall"), KITCHEN(6, "Kitchen"),
	LIBRARY(7, "Library"), LOUNGE(8, "Lounge"), STUDY(9, "Study"),
	CANDLESTICK(10, "Candlestick"), KNIFE(11, "Knife"), LEADPIPE(12, "Leadpipe"),
	REVOLVER(13, "Revolver"), ROPE(14, "Rope"), WRENCH(15, "Wrench"),
	GREEN(16, "Green"), MUSTARD(17, "Mustard"), PEACOCK(18, "Peacock"),
	PLUM(19, "Plum"), SCARLETT(20, "Scarlett"), WHITE(21, "White");
	
	private final int RankValue;
	private final String rankString;
	private Rank(int RankValue, String rankString)
	{
		this.RankValue = RankValue;
		this.rankString = rankString;
	}
	public int getRank()
	{
		return RankValue;
	}
	public String printRank()
	{
		return rankString;
	}
}
