package cards;

import java.util.ArrayList;

public class Suggestions extends CreateDeck 
{
	
	public String compareGuess(int turnCounter, Card card1, Card card2, Card card3, ArrayList<Hand> AllHands) 
	{
		int a = 0;
		for (int i =0; i <= AllHands.size(); i++)
		{
			turnCounter = ((turnCounter + 1) % AllHands.size());
			a++;
			if (AllHands.get(turnCounter).containsCard(card1) || 
				AllHands.get(turnCounter).containsCard(card2) ||
				AllHands.get(turnCounter).containsCard(card3))
			{
				if (AllHands.get(turnCounter).containsCard(card1) && 
						AllHands.get(turnCounter).containsCard(card2) &&
						AllHands.get(turnCounter).containsCard(card3) 
						&& a == AllHands.size())
				{
					return("You guessed " + card1 + card2 + card3 + "but you have these cards.");
				}
				else if (AllHands.get(turnCounter).containsCard(card1) && AllHands.get(turnCounter).containsCard(card2)
						&& a == AllHands.size())
				{
					return("You guessed " + card1 + card2 + "but you have these cards.");
				}
				else if (AllHands.get(turnCounter).containsCard(card1) && AllHands.get(turnCounter).containsCard(card3)
						&& a == AllHands.size())
				{
					return("You guessed " + card1 + card3 + "but you have these cards.");
				}
				else if (AllHands.get(turnCounter).containsCard(card2) && AllHands.get(turnCounter).containsCard(card3)
						&& a == AllHands.size())
				{
					return("You guessed " + card2 + card3 + "but you have these cards.");
				}
				else if (AllHands.get(turnCounter).containsCard(card1) && a == AllHands.size())
				{
					return("You guessed " + card1 + "but you have this card.");
				}
				else if (AllHands.get(turnCounter).containsCard(card2) && a == AllHands.size())
				{
					return("You guessed " + card2 + "but you have this card.");
				}
				else if (AllHands.get(turnCounter).containsCard(card3) && a == AllHands.size())
				{
					return("You guessed " + card3 + "but you have this card.");
				}
				else if (AllHands.get(turnCounter).containsCard(card1) && 
				AllHands.get(turnCounter).containsCard(card2) &&
				AllHands.get(turnCounter).containsCard(card3))
				{
					return(card1.toString() + card2.toString() + card3.toString());
				}
				else if (AllHands.get(turnCounter).containsCard(card1) && AllHands.get(turnCounter).containsCard(card2)) 
				{
					return(card1.toString() + card2.toString());
				}
				else if (AllHands.get(turnCounter).containsCard(card1) && AllHands.get(turnCounter).containsCard(card3))
				{
					return(card1.toString() + card3.toString());
				}
				else if (AllHands.get(turnCounter).containsCard(card2) && AllHands.get(turnCounter).containsCard(card3))
				{
					return(card2.toString() + card3.toString());
				}
				else if (AllHands.get(turnCounter).containsCard(card1))
				{
					return(card1.toString());
				}
				else if (AllHands.get(turnCounter).containsCard(card2))
				{
					return(card2.toString());
				}
				else if (AllHands.get(turnCounter).containsCard(card3))
				{
					return(card3.toString());
				}
				
				
			}
			
		}
		return "You win!!";
	}
}
