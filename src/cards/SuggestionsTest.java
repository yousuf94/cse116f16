package cards;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class SuggestionsTest extends Suggestions {

	@Test
	public void nextPlayerhasPlayerCard() 
	{
		int turnCounter = 2;
		CreateDeck mainDeck = new CreateDeck();
		mainDeck.FillDeck();
		Hand hand1 = new Hand();
		Hand hand2 = new Hand();
		Hand hand3 = new Hand();
		hand1.add(0, mainDeck.getCard(0));
		hand1.add(1, mainDeck.getCard(9));
		hand1.add(2, mainDeck.getCard(15));
		hand2.add(0, mainDeck.getCard(1));
		hand2.add(1, mainDeck.getCard(10));
		hand2.add(2, mainDeck.getCard(16));
		hand3.add(0, mainDeck.getCard(2));
		hand3.add(1, mainDeck.getCard(11));
		hand3.add(2, mainDeck.getCard(17));
		ArrayList<Hand> TheseHands = new ArrayList<Hand>();
		TheseHands.add(hand1);
		TheseHands.add(hand2);
		TheseHands.add(hand3);
		String expected = "GreenofPlayers\n";
		String actual = compareGuess(turnCounter, mainDeck.getCard(3), mainDeck.getCard(13), mainDeck.getCard(15), TheseHands); 
		assertEquals(expected, actual);
	}
	
	@Test
	public void nextPlayerhasRoomCard() 
	{
		int turnCounter = 0;
		CreateDeck mainDeck = new CreateDeck();
		mainDeck.FillDeck();
		Hand hand1 = new Hand();
		Hand hand2 = new Hand();
		Hand hand3 = new Hand();
		hand1.add(0, mainDeck.getCard(6));
		hand1.add(1, mainDeck.getCard(11));
		hand1.add(2, mainDeck.getCard(20));
		hand2.add(0, mainDeck.getCard(4));
		hand2.add(1, mainDeck.getCard(9));
		hand2.add(2, mainDeck.getCard(15));
		hand3.add(0, mainDeck.getCard(6));
		hand3.add(1, mainDeck.getCard(13));
		hand3.add(2, mainDeck.getCard(17));
		ArrayList<Hand> TheseHands = new ArrayList<Hand>();
		TheseHands.add(hand1);
		TheseHands.add(hand2);
		TheseHands.add(hand3);
		String expected = "HallofRooms\n";
		String actual = compareGuess(turnCounter, mainDeck.getCard(4), mainDeck.getCard(13), mainDeck.getCard(20), TheseHands); 
		assertEquals(expected, actual);
	}
	
	@Test
	public void nextPlayerhasWeaponCard() 
	{
		int turnCounter = 1;
		CreateDeck mainDeck = new CreateDeck();
		mainDeck.FillDeck();
		Hand hand1 = new Hand();
		Hand hand2 = new Hand();
		Hand hand3 = new Hand();
		hand1.add(0, mainDeck.getCard(3));
		hand1.add(1, mainDeck.getCard(13));
		hand1.add(2, mainDeck.getCard(19));
		hand2.add(0, mainDeck.getCard(5));
		hand2.add(1, mainDeck.getCard(14));
		hand2.add(2, mainDeck.getCard(16));
		hand3.add(0, mainDeck.getCard(8));
		hand3.add(1, mainDeck.getCard(11));
		hand3.add(2, mainDeck.getCard(17));
		ArrayList<Hand> TheseHands = new ArrayList<Hand>();
		TheseHands.add(hand1);
		TheseHands.add(hand2);
		TheseHands.add(hand3);
		String expected = "LeadpipeofWeapons\n";
		String actual = compareGuess(turnCounter, mainDeck.getCard(3), mainDeck.getCard(11), mainDeck.getCard(16), TheseHands); 
		assertEquals(expected, actual);
	}
	
	@Test
	public void nextPlayerhasTwoMatches() 
	{
		int turnCounter = 2;
		CreateDeck mainDeck = new CreateDeck();
		mainDeck.FillDeck();
		Hand hand1 = new Hand();
		Hand hand2 = new Hand();
		Hand hand3 = new Hand();
		hand1.add(0, mainDeck.getCard(3));
		hand1.add(1, mainDeck.getCard(13));
		hand1.add(2, mainDeck.getCard(19));
		hand2.add(0, mainDeck.getCard(5));
		hand2.add(1, mainDeck.getCard(14));
		hand2.add(2, mainDeck.getCard(16));
		hand3.add(0, mainDeck.getCard(8));
		hand3.add(1, mainDeck.getCard(11));
		hand3.add(2, mainDeck.getCard(17));
		ArrayList<Hand> TheseHands = new ArrayList<Hand>();
		TheseHands.add(hand1);
		TheseHands.add(hand2);
		TheseHands.add(hand3);
		String expected = "DiningRoomofRooms\n" + "RopeofWeapons\n";
		String actual = compareGuess(turnCounter, mainDeck.getCard(3), mainDeck.getCard(13), mainDeck.getCard(16), TheseHands); 
		assertEquals(expected, actual);
	}
	
	@Test
	public void nextnextPlayerhasThreeMatches() 
	{
		int turnCounter = 0;
		CreateDeck mainDeck = new CreateDeck();
		mainDeck.FillDeck();
		Hand hand1 = new Hand();
		Hand hand2 = new Hand();
		Hand hand3 = new Hand();
		hand1.add(0, mainDeck.getCard(3));
		hand1.add(1, mainDeck.getCard(13));
		hand1.add(2, mainDeck.getCard(19));
		hand2.add(0, mainDeck.getCard(5));
		hand2.add(1, mainDeck.getCard(14));
		hand2.add(2, mainDeck.getCard(16));
		hand3.add(0, mainDeck.getCard(8));
		hand3.add(1, mainDeck.getCard(11));
		hand3.add(2, mainDeck.getCard(17));
		ArrayList<Hand> TheseHands = new ArrayList<Hand>();
		TheseHands.add(hand1);
		TheseHands.add(hand2);
		TheseHands.add(hand3);
		String expected = "KitchenofRooms\n" + "WrenchofWeapons\n" + "MustardofPlayers\n";
		String actual = compareGuess(turnCounter, mainDeck.getCard(5), mainDeck.getCard(14), mainDeck.getCard(16), TheseHands); 
		assertEquals(expected, actual);
	}
	
	@Test
	public void lastPlayerhasTwoMatches() 
	{
		int turnCounter = 0;
		CreateDeck mainDeck = new CreateDeck();
		mainDeck.FillDeck();
		Hand hand1 = new Hand();
		Hand hand2 = new Hand();
		Hand hand3 = new Hand();
		Hand hand4 = new Hand();
		Hand hand5 = new Hand();
		hand1.add(0, mainDeck.getCard(3));
		hand1.add(1, mainDeck.getCard(13));
		hand1.add(2, mainDeck.getCard(18));
		hand2.add(0, mainDeck.getCard(5));
		hand2.add(1, mainDeck.getCard(14));
		hand2.add(2, mainDeck.getCard(16));
		hand3.add(0, mainDeck.getCard(2));
		hand3.add(1, mainDeck.getCard(12));
		hand3.add(2, mainDeck.getCard(19));
		hand4.add(0, mainDeck.getCard(6));
		hand4.add(1, mainDeck.getCard(11));
		hand4.add(2, mainDeck.getCard(17));
		hand5.add(0, mainDeck.getCard(7));
		hand5.add(1, mainDeck.getCard(9));
		hand5.add(2, mainDeck.getCard(20));
		ArrayList<Hand> TheseHands = new ArrayList<Hand>();
		TheseHands.add(hand1);
		TheseHands.add(hand2);
		TheseHands.add(hand3);
		TheseHands.add(hand4);
		TheseHands.add(hand5);
		String expected = "LoungeofRooms\n" + "WhiteofPlayers\n";
		String actual = compareGuess(turnCounter, mainDeck.getCard(7), mainDeck.getCard(10), mainDeck.getCard(20), TheseHands);
		assertEquals(expected, actual);
	}
	
	@Test
	public void noPlayerHasMatches() 
	{
		int turnCounter = 0;
		CreateDeck mainDeck = new CreateDeck();
		mainDeck.FillDeck();
		Hand hand1 = new Hand();
		Hand hand2 = new Hand();
		Hand hand3 = new Hand();
		Hand hand4 = new Hand();
		Hand hand5 = new Hand();
		hand1.add(0, mainDeck.getCard(3));
		hand1.add(1, mainDeck.getCard(13));
		hand1.add(2, mainDeck.getCard(18));
		hand2.add(0, mainDeck.getCard(5));
		hand2.add(1, mainDeck.getCard(14));
		hand2.add(2, mainDeck.getCard(16));
		hand3.add(0, mainDeck.getCard(2));
		hand3.add(1, mainDeck.getCard(12));
		hand3.add(2, mainDeck.getCard(19));
		hand4.add(0, mainDeck.getCard(6));
		hand4.add(1, mainDeck.getCard(11));
		hand4.add(2, mainDeck.getCard(17));
		hand5.add(0, mainDeck.getCard(7));
		hand5.add(1, mainDeck.getCard(9));
		hand5.add(2, mainDeck.getCard(20));
		ArrayList<Hand> TheseHands = new ArrayList<Hand>();
		TheseHands.add(hand1);
		TheseHands.add(hand2);
		TheseHands.add(hand3);
		TheseHands.add(hand4);
		TheseHands.add(hand5);
		String expected = "You win!!";
		String actual = compareGuess(turnCounter, mainDeck.getCard(0), mainDeck.getCard(10), mainDeck.getCard(15), TheseHands);
		assertEquals(expected, actual);
	}
	
	@Test
	public void onlyGuesserHasMatches()
	{
		int turnCounter = 0;
		CreateDeck mainDeck = new CreateDeck();
		mainDeck.FillDeck();
		Hand hand1 = new Hand();
		Hand hand2 = new Hand();
		Hand hand3 = new Hand();
		Hand hand4 = new Hand();
		Hand hand5 = new Hand();
		hand1.add(0, mainDeck.getCard(3));
		hand1.add(1, mainDeck.getCard(13));
		hand1.add(2, mainDeck.getCard(18));
		hand2.add(0, mainDeck.getCard(5));
		hand2.add(1, mainDeck.getCard(14));
		hand2.add(2, mainDeck.getCard(16));
		hand3.add(0, mainDeck.getCard(2));
		hand3.add(1, mainDeck.getCard(12));
		hand3.add(2, mainDeck.getCard(19));
		hand4.add(0, mainDeck.getCard(6));
		hand4.add(1, mainDeck.getCard(11));
		hand4.add(2, mainDeck.getCard(17));
		hand5.add(0, mainDeck.getCard(7));
		hand5.add(1, mainDeck.getCard(9));
		hand5.add(2, mainDeck.getCard(20));
		ArrayList<Hand> TheseHands = new ArrayList<Hand>();
		TheseHands.add(hand1);
		TheseHands.add(hand2);
		TheseHands.add(hand3);
		TheseHands.add(hand4);
		TheseHands.add(hand5);
		String expected = "You guessed " + "DiningRoomofRooms\n" + "RopeofWeapons\n" + "but you have these cards.";
		String actual = compareGuess(turnCounter, mainDeck.getCard(3), mainDeck.getCard(13), mainDeck.getCard(15), TheseHands);
		assertEquals(expected, actual);
	}
}
