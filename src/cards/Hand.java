package cards;

import java.util.ArrayList;

public class Hand 
{
	private ArrayList<Card> cards;
	public Hand()
	{
		cards = new ArrayList<Card>();
	}
	public void add(int index, Card card)
	{
		cards.add(index, card);
	}
	public Card getCard(int index)
	{
		return cards.get(index);
	}
	public boolean containsCard(Card card)
	{
		if (cards.contains(card))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean give(int index, Hand otherHand, int idx)
	{
			otherHand.add(idx, cards.remove(index));
			return true;
	}
	public String displayHand()
	{
		String  b = "";
		for (Card card : cards)
		{
			b += card.toString();
		}
		return b;
	}
}
