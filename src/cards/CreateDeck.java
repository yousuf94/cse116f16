package cards;

public class CreateDeck extends Hand
{
	public void FillDeck()
	{
		Card card1, card2, card3, card4, card5,
		card6, card7, card8, card9, card10,
		card11, card12, card13, card14, card15,
		card16, card17, card18, card19, card20,
		card21;
		
		card1 = new Card(Rank.BALLROOM, Suit.ROOMS);
		card2 = new Card(Rank.BILLIARDROOM, Suit.ROOMS);
		card3 = new Card(Rank.CONSERVATORY, Suit.ROOMS);
		card4 = new Card(Rank.DININGROOM, Suit.ROOMS);
		card5 = new Card(Rank.HALL, Suit.ROOMS);
		card6 = new Card(Rank.KITCHEN, Suit.ROOMS);
		card7 = new Card(Rank.LIBRARY, Suit.ROOMS);
		card8 = new Card(Rank.LOUNGE, Suit.ROOMS);
		card9 = new Card(Rank.STUDY, Suit.ROOMS);
		card10 = new Card(Rank.CANDLESTICK, Suit.WEAPONS);
		card11 = new Card(Rank.KNIFE, Suit.WEAPONS);
		card12 = new Card(Rank.LEADPIPE, Suit.WEAPONS);
		card13 = new Card(Rank.REVOLVER, Suit.WEAPONS);
		card14 = new Card(Rank.ROPE, Suit.WEAPONS);
		card15 = new Card(Rank.WRENCH, Suit.WEAPONS);
		card16 = new Card(Rank.GREEN, Suit.PLAYERS);
		card17 = new Card(Rank.MUSTARD, Suit.PLAYERS);
		card18 = new Card(Rank.PEACOCK, Suit.PLAYERS);
		card19 = new Card(Rank.PLUM, Suit.PLAYERS);
		card20 = new Card(Rank.SCARLETT, Suit.PLAYERS);
		card21 = new Card(Rank.WHITE, Suit.PLAYERS);
		
		this.add(0, card1);
		this.add(1, card2);
		this.add(2, card3);
		this.add(3, card4);
		this.add(4, card5);
		this.add(5, card6);
		this.add(6, card7);
		this.add(7, card8);
		this.add(8, card9);
		this.add(9, card10);
		this.add(10, card11);
		this.add(11, card12);
		this.add(12, card13);
		this.add(13, card14);
		this.add(14, card15);
		this.add(15, card16);
		this.add(16, card17);
		this.add(17, card18);
		this.add(18, card19);
		this.add(19, card20);
		this.add(20, card21);
	}
}
