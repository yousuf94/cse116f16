package cards;

public class Card 
{
	private Suit suit;
	private Rank rank;
	public Card(Rank rank, Suit suit)
	{
		this.rank = rank;
		this.suit = suit;
	}
	public String getSuit()
	{
		return suit.printSuit();
	}
	public int getRank()
	{
		return rank.getRank();
	}
	public String toString()
	{
		String a = "";
		a += rank.printRank() + "of" + suit.printSuit() + "\n";
		return a;
	}
}
