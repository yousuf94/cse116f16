package model;

import static org.junit.Assert.*;

import org.junit.Test;


public class PlayerTest extends Player
{
	@Test
	public void HorizontalOnlyTest()
	{
		Player Player1 = new Player("Player1", board[6][17]);
		int[] path = new int[5];
		int[] Keystrokes = new int[4];
		Keystrokes[0] = Player1.keyPressed(37);
		Keystrokes[1] = Player1.keyPressed(37);
		Keystrokes[2] = Player1.keyPressed(37);
		Keystrokes[3] = Player1.keyPressed(39);
		
		path[0] = board[6][17];
		path[1] = Keystrokes[0];
		path[2] = Keystrokes[1];
		path[3] = Keystrokes[2];
		path[4] = Keystrokes[3];
		
		int counter = 0;
		int dieRoll = 4;
		while (dieRoll > 0)
		{
			for (int i = 0; i < path.length - 1; i++)
			{
				Player1.getLocation();
				boolean a = moveIsHorizontallyLegal(path[i], path[i+1]);
				if (a == true)
				{
					counter+=1;
					dieRoll -=1;
				}
				else if (a == false)
				{
					counter+=0;
				}
			}
		}
		int expected = 4;
		assertEquals(expected, counter);
	}
	
	@Test
	public void VerticalOnlyTest()
	{
		Player Player1 = new Player("Player1", board[0][7]);
		Player1.getLocation();
		int[] Keystrokes = new int[3];
		Keystrokes[0] = Player1.keyPressed(40);
		Keystrokes[1] = Player1.keyPressed(40);
		Keystrokes[2] = Player1.keyPressed(38);
		int[] path = new int[4];
		path[0] = board[0][7];
		path[1] = Keystrokes[0];
		path[2] = Keystrokes[1];
		path[3] = Keystrokes[2];
		int counter = 0;
		int dieRoll = 3;
		while (dieRoll > 0)
		{
			for (int i = 0; i < Keystrokes.length; i++)
			{
				boolean a = moveIsVerticallyLegal(path[i], path[i+1]);
				if (a == true)
				{
					counter+=1;
					dieRoll--;
				}
				else if (a == false)
				{
					counter+=0;
				}
			}
		}
		int expected = 3;
		assertEquals(expected, counter, 0);
	}
	
	@Test
	public void HorizontalandVerticalTest()
	{
		Player Player1 = new Player("Player1", board[9][7]);
		Player1.getLocation();
		int[] Keystrokes = new int[4];
		Keystrokes[0] = Player1.keyPressed(40);
		Keystrokes[1] = Player1.keyPressed(40);
		Keystrokes[2] = Player1.keyPressed(39);
		Keystrokes[3] = Player1.keyPressed(39);
		int[] path = new int[5];
		path[0] = board[9][7];
		path[1] = Keystrokes[0];
		path[2] = Keystrokes[1];
		path[3] = Keystrokes[2];
		path[4] = Keystrokes[3];
		int counter = 0;
		int dieRoll = 4;
		while (dieRoll > 0)
		for (int i = 0; i < Keystrokes.length; i++)
		{
			boolean a = moveIsLegal(path[i], path[i+1]);
			
			if (a == true)
			{
				location = Keystrokes[i];
				Player1.updateLocation(location);
				counter+=1;
				dieRoll--;
			}
			else 
			{
				counter+=0;
			}
		}
		assertEquals(4, counter);
	}
	
	@Test public void diagonalMoveIsIllegal()
	{
		//A diagonal move is impossible in our version of the game, because keystrokes determine how a player moves.
		//So, it is impossible for a player to move to a diagonal location in one step. They would have to use exactly
		//two steps. 
	}
	
	@Test public void noncontiguousMoveIsIllegal()
	{
		//This type of move is also impossible. Because our version of the game defines moves based on keystrokes,
		//each step a player takes must reach an adjacent square on the board. 
	}
	
	@Test public void properStepsTest()
	{
		Player Player1 = new Player("Player1", board[0][10]);
		int dieRoll = 3;
		int[] Keystrokes = new int[4];
		Keystrokes[0] = Player1.keyPressed(37);
		Keystrokes[1] = Player1.keyPressed(38);
		Keystrokes[2] = Player1.keyPressed(39);
		Keystrokes[3] = Player1.keyPressed(40);
		int counter = 0;
		for (int i = 0; i < Keystrokes.length; i++)
		{
			counter = Player1.stepsCounter(Keystrokes[i]);
		}
		
		boolean a = Player1.isProperSteps(dieRoll, counter);
		assertFalse(a);
	}
	
	//Test Player enters a Room through a Door (the Study, specifically)
	@Test public void playerEntersStudy()
	{
		Player Player1 = new Player("Player1", board[4][6]);
		int[] Keystrokes = new int[6];
		Keystrokes[0] = Player1.keyPressed(37);
		Keystrokes[1] = Player1.keyPressed(37);
		Keystrokes[2] = Player1.keyPressed(37);
		Keystrokes[3] = Player1.keyPressed(37);
		Keystrokes[4] = Player1.keyPressed(37);
		Keystrokes[5] = Player1.keyPressed(38);
		int[] path = new int[7];
		path[0] = board[4][6];
		path[1] = Keystrokes[0];
		path[2] = Keystrokes[1];
		path[3] = Keystrokes[2];
		path[4] = Keystrokes[3];
		path[5] = Keystrokes[4];
		path[6] = Keystrokes[5];
		int dieRoll = 6;
		int counter = 0;
		while (dieRoll > 0)
		{
			for (int i = 0; i < Keystrokes.length; i++)
			{
				boolean a = moveIsLegal(path[i], path[i+1]);
				if (a == true)
				{
					counter+=1;
					dieRoll--;
				}
				else if (a == false)
				{
					counter+=0;
				}
			}
		}
		int expected = 6;
		assertEquals(expected, counter);
	}
	
	//Player moves down legally, down legally, left legally, left legally, then left and hits a wall
	@Test public void playerHitsWall()
	{
		Player Player1 = new Player("Player1", board[9][7]);
		Player1.getLocation();
		int[] Keystrokes = new int[5];
		Keystrokes[0] = Player1.keyPressed(40);
		Keystrokes[1] = Player1.keyPressed(40);
		Keystrokes[2] = Player1.keyPressed(37);
		Keystrokes[3] = Player1.keyPressed(37);
		Keystrokes[4] = Player1.keyPressed(37);
		int[] path = new int[6];
		path[0] = board[9][7];
		path[1] = Keystrokes[0];
		path[2] = Keystrokes[1];
		path[3] = Keystrokes[2];
		path[4] = Keystrokes[3];
		path[5] = Keystrokes[4];

		int counter = 0;
		for (int i = 0; i < Keystrokes.length; i++)
		{
			boolean a = moveIsLegal(path[i], path[i+1]);
			if (a == true)
			{
				location = Keystrokes[i];
				Player1.updateLocation(location);
				counter+=1;
			}
			else if (a == false)
			{
				counter+=0;
			}
		}
		assertEquals(4, counter);
	}
	
	//Player is located in Kitchen and presses S key to use Secret Passage to Study
	@Test public void playerUsesSecretPassage()
	{
		Player Player1 = new Player("Player1", board [17][17]);
		Player1.getLocation();
		int[] Keystrokes = new int[1];
		location = Keystrokes[0];
		Player1.updateLocation(location);
		assertEquals(0, Player1.getLocation());
	}
}
