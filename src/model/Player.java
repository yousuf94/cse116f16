package model;


import java.awt.event.KeyEvent;
import java.util.ArrayList;

public class Player extends Board
{
	private String name;
	public int location;
	int steps = 0;
    int dieRoll = 0;
    ArrayList<Integer> locations = new ArrayList<Integer>();
    
    public Player()
    {
    	String name;
    	int location;
    }
    public Player(String getName, int getLocation)
	{
		this.name = getName;
		this.location = getLocation;
	}
    
	public String getName()
	{
		return name;
	}
	public void setLocation(int location)
	{
		this.location = location;
	}
	public int getLocation()
	{
		return location;
	}
	public int updateLocation(int l)
	{
		location = l;
		return location;
	}
   
	public boolean moveIsHorizontallyLegal(int locationOfPlayer, int newLocation)
    {
		if (!borders.contains(newLocation))
		{
    	    if(newLocation - locationOfPlayer == 1)
    	    {
    	    	return true;
    	    }
    	    else if (newLocation - locationOfPlayer == -1)
    	    {
    	    	return true;
    	    }
    	    else
    	    {
    	    	return false;
    	    }
    	  }
    	  return false;
    }
   
    public int keyPressed(int event)
    {
    	if (event == KeyEvent.VK_LEFT)
    	{
    		location = location - 1;
    	}
    	if (event == KeyEvent.VK_UP)
    	{
    		location = location - 20;
    	}
    	if (event == KeyEvent.VK_RIGHT)
    	{
    		location = location + 1;
    	}
    	if (event == KeyEvent.VK_DOWN)
    	{
    		location = location + 20;
    	}
    	return location;	
    }
    
    public int secretPassagePressed(int event, int locationOfPlayer)
    {
    	if (event == KeyEvent.VK_S)
    	{
    		if (Secretpassages1.contains(locationOfPlayer))
    		{
    			location = 0;
    		}
    		if(Secretpassages2.contains(locationOfPlayer))
    		{
    			location = 19;
    		}
    	
    		if(Secretpassages3.contains(locationOfPlayer))
    		{
    			location = 380;
    		}
    		
    		if(Secretpassages4.contains(locationOfPlayer))
    		{
    			location = 399;
    		}
    	}
		return location;
    }
    
    public int stepsCounter(int event)
    {
    	if ((event == KeyEvent.VK_LEFT) || (event == KeyEvent.VK_UP) || 
    	(event == KeyEvent.VK_RIGHT) || (event == KeyEvent.VK_DOWN))
    	{
    		steps++;
    	}
    	return steps;

    }
    
    public boolean isProperSteps (int dieRoll, int steps)
    {
    	if (dieRoll <= steps)
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }
    
    public boolean moveIsVerticallyLegal(int locationOfPlayer, int newLocation)
    {
    	if (!borders.contains(newLocation))
    	{
    		if(newLocation - locationOfPlayer == 20)
    		{
    			return true;
    		}
    		else if (newLocation - locationOfPlayer == -20)
    		{
    			return true;
    		}
    		else
    		{
    			return false;
    		}
    	}
    	return false;
    }
   
   
    public boolean moveIsLegal(int locationOfPlayer, int newLocation){
       
        if (moveIsHorizontallyLegal(locationOfPlayer, newLocation) || (moveIsVerticallyLegal(locationOfPlayer, newLocation)))
        {   
            return true;
           
        }
        return false;
    }
}

