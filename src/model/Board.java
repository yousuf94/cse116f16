package model;

import java.util.ArrayList;

public class Board {

	
	
	int [][] board = new int [20][20];
	
	//6 entrances
	//9 rooms
	//an entrance/room is represented by an address
	
	//entrances
	int ent1;
	int ent2;
	int ent3;
	int ent4;
	int ent5;
	int ent6;
	
	//9 rooms
	int[][] library;
	int libraryDoor = 184;
	int[][] studyRoom;
	int studyRoomDoor = 81;
	int[][] hall; 
	int hallDoor = 108;
	int[][] lounge;
	int loungeDoor = 136;
	int[][] diningRoom;
	int diningRoomDoor1 = 152;
	int diningRoomDoor2 = 169;
	int[][] kitchen;
	int kitchenDoor = 317;
	int[][] ballRoom;
	int ballRoomDoor1 = 259;
	int ballRoomDoor2 = 331;
	int[][] billiardRoom;
	int billiardRoomDoor1 = 185;
	int billiardRoomDoor2 = 247;
	int[][] conservatory;
	int conservatoryRoomDoor = 281;
	
	ArrayList<Integer> doorsArrayList = new ArrayList<Integer>();
	ArrayList<Integer> borders = new ArrayList<Integer>();
	ArrayList<Integer> Secretpassages1 = new ArrayList<Integer>();
	ArrayList<Integer> Secretpassages2 = new ArrayList<Integer>();
	ArrayList<Integer> Secretpassages3 = new ArrayList<Integer>();
	ArrayList<Integer> Secretpassages4 = new ArrayList<Integer>();
	
	
	
	public Board()
	{
		//fill board
		int counter = 0;
		for (int i =0; i <  board.length; i++)
		{
			for (int j = 0; j < board.length; j++)
			{
				board[i][j] = counter;
				counter++;
			}
		}
		//fill in doorsArrayList
		doorsArrayList.add(281);
		doorsArrayList.add(185);
		doorsArrayList.add(246);
		doorsArrayList.add(259);
		doorsArrayList.add(331);
		doorsArrayList.add(317);
		doorsArrayList.add(169);
		doorsArrayList.add(152);
		doorsArrayList.add(136);
		doorsArrayList.add(108);
		doorsArrayList.add(81);
		doorsArrayList.add(184);
		
		//fill in Secret Passages squares
		//study room(4x7)
		for(int x = 0; x < 4; x++){
		 for(int y = 0; y < 7; y++){
		            Secretpassages1.add(board[x][y]);
		            //System.out.println(studyRoom[x][y]);
		        }
		      
		    }
		  
		//Lounge(6x7)
		  
		    for(int x = 0; x < 6; x++){
		        for(int y = 13; y < 20; y++){
		            Secretpassages2.add(board[x][y]);
		              
		            }
		          
		        }
		  
		        //Conservatory
		    	for (int x = 14; x < 20; x++)
		    	{
		    		for (int y = 0; y < 6; y++)
		    		{
		    			Secretpassages3.add(board[x][y]);
		    		}
		    	}
		  
		        // Kitchen
		      
		        for(int x = 15; x < 20; x++){
		            for(int y = 15; y < 20; y++){
		                Secretpassages4.add(board[x][y]);
		            }
		 	borders.add(0);
	        borders.add(1);
	        borders.add(2);
	        borders.add(3);
	        borders.add(4);
	        borders.add(6);
	        borders.add(26);
	        borders.add(46);
	        borders.add(66);
	        borders.add(60);
	        borders.add(62);
	        borders.add(63);
	        borders.add(64);
	        borders.add(65);
	        borders.add(100);
	        borders.add(101);
	        borders.add(102);
	        //102       
	        borders.add(103);
	        borders.add(104);
	        borders.add(105);
	        borders.add(106);
	        borders.add(126);
	        borders.add(146);
	        borders.add(166);
	        borders.add(160);
	        borders.add(161);
	        borders.add(162);
	        borders.add(163);
	        borders.add(165);
	        borders.add(200);
	        borders.add(201);
	        borders.add(202);
	        borders.add(203);
	        borders.add(204);
	        //204
	      
	        borders.add(260);
	        borders.add(261);
	        borders.add(262);
	        borders.add(263);
	        borders.add(264);
	        borders.add(265);
	        borders.add(225);
	        borders.add(245);
	        borders.add(300);
	        borders.add(302);
	        borders.add(303);
	        borders.add(304);
	        borders.add(305);
	        borders.add(325);
	        borders.add(345);
	        borders.add(365);
	        borders.add(385);
	        borders.add(380);
	        borders.add(381);
	        borders.add(382);
	        borders.add(383);
	        borders.add(384);
	        borders.add(267);
	        borders.add(268);
	        borders.add(270);
	        borders.add(290);
	        borders.add(310);
	        borders.add(330);
	        borders.add(350);
	        borders.add(370);
	        borders.add(390);
	        borders.add(388);
	        borders.add(389);
	        borders.add(390);
	        borders.add(391);
	        borders.add(13);
	        borders.add(14);
	        borders.add(15);
	        borders.add(16);
	        borders.add(17);
	        //17
	        borders.add(18);
	        borders.add(19);
	        borders.add(113);
	        borders.add(114);
	        borders.add(115);
	        borders.add(116);
	        borders.add(117);
	        borders.add(119);
	        borders.add(33);
	        borders.add(53);
	        borders.add(73);
	        borders.add(93);
	        borders.add(113);
	        borders.add(8);
	        borders.add(9);
	        borders.add(10);
	        borders.add(89);
	        borders.add(90);
	        borders.add(30);
	        borders.add(50);
	        borders.add(70);
	        borders.add(28);
	        borders.add(48);
	        borders.add(68);
	        borders.add(336);
	        borders.add(356);
	        borders.add(376);
	        borders.add(338);
	        borders.add(339);
	        borders.add(396);
	        borders.add(397);
	        borders.add(398);
	        borders.add(399);
	        borders.add(26);
	        borders.add(46);
	        borders.add(126);
	        borders.add(146);
	        borders.add(225);
	        borders.add(245);
	        borders.add(325);
	        borders.add(345);
	        borders.add(365);
	        borders.add(287);
	        //287
	        borders.add(307);
	        borders.add(327);
	        borders.add(347);
	        borders.add(367);
	        borders.add(290);
	        borders.add(310);
	        borders.add(330);
	        borders.add(350);
	        borders.add(370);
	        borders.add(390);
	        borders.add(33);
	        borders.add(53);
	        borders.add(73);
	        borders.add(93);
	        borders.add(39);
	        borders.add(59);
	        borders.add(79);
	        borders.add(99);
	        borders.add(337);
	        borders.add(338);
	        borders.add(356);
	        borders.add(376);
	        //376
	        borders.add(26);
	        borders.add(46);
	        borders.add(126);
	        borders.add(146);
	        borders.add(225);
	        borders.add(325);
	        borders.add(345);
	        borders.add(365);
	        borders.add(287);
	        
	        borders.add(173);
	        borders.add(174);
	        borders.add(175);
	        borders.add(176);
	        borders.add(177);
	        borders.add(178);
	        borders.add(179);
	        borders.add(292);
	        borders.add(293);
	        borders.add(294);
	        borders.add(295);
	        borders.add(296);
	        borders.add(297);
	        borders.add(298);
	        borders.add(299);
	        borders.add(192);
	        borders.add(212);
	        borders.add(232);
	        borders.add(252);
	        borders.add(272);
	        borders.add(292);
	        
	        
	        
		    }
	}
}



	